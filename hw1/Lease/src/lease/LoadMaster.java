package lease;

import java.io.*;
import java.net.*;

public class LoadMaster {
	public static void main(String[] args) throws IOException {
	MulThrServer server = new MulThrServer(10000);
	new Thread(server).start();
	
	Client client = new Client(10000);
	new Thread(client).start();
	
	//Client client2 = new Client(10000);
	//new Thread(client2).start();

	try {
	    Thread.sleep(20 * 1000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	System.out.println("Stopping Server");
	server.stop();
	}
}
