package lease;

import java.io.Serializable;

public class MessageUpdate implements Serializable{
	public int key;
	public int value;
	public boolean rw;//r:true. write:false

	public MessageUpdate(int k, int v, boolean r){
		key = k;
		value = v;
		rw = r;
	}
	
	public String toString(){
		return "key = " + key + " value = " + " r/w: " + rw;
	}
}
