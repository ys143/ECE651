package lease;

import java.io.*;
import java.net.*;

public class Client implements Runnable{
	static int port = 10000;
    MessageUpdate m1 = new MessageUpdate(1,1,false);
    private ObjectInputStream ois = null;
    private ObjectOutputStream oos = null;
	
    public Client(int p) {
        port=p;
    }
    
    public void run(){

        String serverHostname = new String ("127.0.0.1");

        System.out.println ("Connecting to host " + serverHostname + " on port "+port);

        Socket echoSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        


        try {
            echoSocket = new Socket(serverHostname, port);
            
            oos = new ObjectOutputStream(echoSocket.getOutputStream());
            //Student student = new Student(1, "Bijoy");
            oos.writeObject(m1);
           
            
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                                        echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("unknown host: " + serverHostname);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for " + "the connection to: " + serverHostname);
            System.exit(1);
        }

	BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
	String userInput;

        System.out.print ("input: ");
        try {
	while (((userInput = stdIn.readLine()) != null )) {
	    out.println(userInput);
	    if(userInput.equals("quit")) break;
	    System.out.println("echo: " + in.readLine());
            System.out.print ("input: ");
	}

	out.close();
	in.close();
	stdIn.close();
	echoSocket.close();
        } catch (IOException e){
        	System.err.println("IO Exception");
            System.exit(1);
        }
        
        
    }
}
