package lease;

import java.io.*;
import java.net.*;

public class ServerWorker implements Runnable{
    protected Socket clientSocket = null;
    protected String serverText   = null;

    public ServerWorker(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText   = serverText;
    }

    public void run() {
        try {
    	    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true); 
    	    BufferedReader in = new BufferedReader(new InputStreamReader( clientSocket.getInputStream())); 
    	    String inputLine; 

    	    while ((inputLine = in.readLine()) != null) { 
    	    	System.out.println ("Server: " + inputLine); 
    	    	out.println(inputLine); 
    	    	if (inputLine.equals("quit")) break; 
    	    } 

    	    out.close(); 
    	    in.close(); 
    	    clientSocket.close(); 
    	    //serverSocket.close(); 
            //InputStream input  = clientSocket.getInputStream();
            //OutputStream output = clientSocket.getOutputStream();
            //long time = System.currentTimeMillis();
            //output.write(("WorkerRunnable: " + this.serverText + " - " + time + "").getBytes());
            //output.close();
            //input.close();
            //System.out.println("Request processed: " + time);
            
        } catch (IOException e) {
            //report exception somewhere.
            e.printStackTrace();
        }
    }

}
