import java.io.*;
import java.net.*;

public class SimpleClient {
	static int port = 10000;
    public static void main(String[] args) throws IOException {

        String serverHostname = new String ("127.0.0.1");

        System.out.println ("Connecting to host " + serverHostname + " on port "+port);

        Socket echoSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            echoSocket = new Socket(serverHostname, port);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                                        echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("unknown host: " + serverHostname);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for " + "the connection to: " + serverHostname);
            System.exit(1);
        }

	BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
	String userInput;

        System.out.print ("input: ");
	while (((userInput = stdIn.readLine()) != null )) {
	    out.println(userInput);
	    if(userInput.equals("quit")) break;
	    System.out.println("echo: " + in.readLine());
            System.out.print ("input: ");
	}

	out.close();
	in.close();
	stdIn.close();
	echoSocket.close();
    }
}
